import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Category {
    private String name;
    private List<Product> products;
    public static Comparator<Category> compareByName = (p1, p2) -> p1.getName().compareToIgnoreCase(p2.getName());

    Category(String name) {
        this.name = name;
        products = new ArrayList<>();
    }

    String getName() {
        return name;
    }

    List<Product> getProductsFromCategory() {
        return products;
    }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                '}';
    }

}
