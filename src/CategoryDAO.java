import java.util.*;

public class CategoryDAO {

    private List<Category> categories = new ArrayList<>();

    void addCategory(Category category) {
        categories.add(category);
    }

    void removeCategory(Category category) {
        categories.remove(category);
    }

    List<Category> getCategories() {
        return categories;
    }

}
