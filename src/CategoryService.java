import java.util.List;

public class CategoryService {
    private final CategoryDAO categoryDAO;

    public CategoryService(CategoryDAO dao) {
        this.categoryDAO = dao;
    }

    public void addCategory(String name) {
        if (!isCategory(name)) {
            categoryDAO.addCategory(createCategory(name));
        } else throw new IllegalArgumentException();
    }

    private boolean isCategory(String name) {
        return getAllCategories().stream()
                .anyMatch(category -> category.getName().equals(name));
    }

    private Category createCategory(String name) {
        return new Category(name);
    }

    public void removeCategory(String name) {
        if (!isCategoryEmpty(name)) {
            throw new RuntimeException();
        }
        categoryDAO.removeCategory(getCategoryByName(name));
    }

    private boolean isCategoryEmpty(String name) {
        return getCategoryByName(name).getProductsFromCategory().isEmpty();
    }

    public Category getCategoryByName(String name) {
        return getAllCategories().stream()
                .filter(category -> category.getName().equals(name))
                .findFirst().orElseThrow(IllegalArgumentException::new);
    }

    public List<Category> getAllCategories() {
        return categoryDAO.getCategories();
    }

    public void sortAllCategoriesByName() {
        getAllCategories().sort(Category.compareByName);
    }

}



