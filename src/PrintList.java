import java.util.List;

public class PrintList implements Print {
    @Override
    public void printList(List<?> list) {
        list.forEach(System.out::println);
    }
}
