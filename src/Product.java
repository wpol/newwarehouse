import java.util.Comparator;

public class Product {
    private String name;
    private int serial;
    public static Comparator<Product> compareByName = (p1, p2) -> p1.getName().compareToIgnoreCase(p2.getName());
    public static Comparator<Product> compareBySerial = Comparator.comparingInt(Product::getSerial);

    Product(String name, int serial) {
        this.name = name;
        this.serial = serial;
    }

    Comparator<Product> bySerial = Comparator.comparingInt(Product::getSerial);

    String getName() {
        return name;
    }

    int getSerial() {
        return serial;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", serial=" + serial +
                '}';
    }

}
