
public class ProductCategoryDAO {

    void addProductToCategory(Category category, Product product) {
        category.getProductsFromCategory().add(product);
    }

    void removeProductFromCategory(Category category, Product product) {
        category.getProductsFromCategory().remove(product);
    }

}
