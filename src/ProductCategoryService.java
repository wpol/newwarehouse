import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class ProductCategoryService {
    private final ProductCategoryDAO productCategoryDAO;
    private final CategoryService categoryService;
    private final ProductService productService;

    public ProductCategoryService(ProductCategoryDAO productCategoryDAO, CategoryService categoryService, ProductService productService) {
        this.productCategoryDAO = productCategoryDAO;
        this.categoryService = categoryService;
        this.productService = productService;
    }

    public void addProductByCategoryNameAndSerial(String categoryName, String productName, int serial) {
        Category category = categoryService.getCategoryByName(categoryName);
        productService.addProductToListOfProductsByNameAndSerial(productName, serial);
        productCategoryDAO.addProductToCategory(category, productService.getProductByNameAndSerial(productName, serial));
    }

    public void removeProductByCategoryAndName(String categoryName, String productName) {
        Category category = categoryService.getCategoryByName(categoryName);
        Product product = getProductByCategoryAndName(categoryName, productName);
        productService.removeProductBySerialFromListofProducts(product.getSerial());
        productCategoryDAO.removeProductFromCategory(category, product);
    }

    public Product getProductByCategoryAndName(String categoryName, String productName) {
        return categoryService.getCategoryByName(categoryName)
                .getProductsFromCategory().stream()
                .filter(p -> p.getName().equals(productName))
                .findFirst().orElseThrow(IllegalArgumentException::new);
    }

    public List<Product> getAllProductsFromCategory(String categoryName) {
        return categoryService.getCategoryByName(categoryName).getProductsFromCategory();
    }

    public void sortAllProductsFromCategoryByName(String categoryName) {
        getAllProductsFromCategory(categoryName)
                .sort(Product.compareByName);
    }

    public void sortAllProductsFromCategoryBySerial(String categoryName) {
        getAllProductsFromCategory(categoryName)
                .sort(Product.compareBySerial);
    }

}
