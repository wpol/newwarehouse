import java.util.ArrayList;
import java.util.List;

public class ProductDAO {

    private List<Product> products = new ArrayList<>();

    void addProductToList(Product product) {
        products.add(product);
    }

    void removeProduct(Product product) {
        products.remove(product);
    }

    List<Product> getProductsFromProductList() {
        return products;
    }
}
