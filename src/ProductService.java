import java.util.Comparator;
import java.util.List;

public class ProductService {
    private final ProductDAO productDAO;

    public ProductService(ProductDAO productDAO) {
        this.productDAO = productDAO;
    }

    void addProductToListOfProductsByNameAndSerial(String name, int serial) {
        productDAO.addProductToList(createProduct(name, serial));
    }

    private Product createProduct(String name, int serial) {
        return new Product(name, serial);
    }

    public Product getProductByNameAndSerial(String name, int serial) {
        return getAllProducts().stream()
                .filter(product -> product.getName().equals(name) && product.getSerial() == serial)
                .findFirst().orElseThrow(IllegalArgumentException::new);
    }

    void removeProductBySerialFromListofProducts(int serial) {
        productDAO.getProductsFromProductList().removeIf(product -> product.getSerial() == serial);
    }

    public List<Product> getAllProducts() {
        return productDAO.getProductsFromProductList();
    }

    public void sortAllProductsByName() {
        getAllProducts().sort(Product.compareByName);
    }

    public void sortAllProductsBySerial() {
        getAllProducts().sort(Product.compareBySerial);
    }

}

